import React from 'react';

export default function PrimerComponente() {
    return (
        <div>
            <h1>Bienvenido a React</h1>
            <p>Para más información dirígete a: <a href="https://es.reactjs.org/">ReactJS</a></p>
        </div>
    )
}
