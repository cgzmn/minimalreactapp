import React from 'react';
import { render } from 'react-dom';

import PrimerComponente from './components/PrimerComponente/PrimerComponente'

render(<PrimerComponente />, document.getElementById('root'));